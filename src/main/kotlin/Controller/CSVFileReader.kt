package Controller

import java.io.File

class CSVFileReader(val fileName:String) {
    fun read(): ArrayList<String>
    {
        val data = ArrayList(File(fileName).readLines())
        data.removeAt(0)
        return data
    }
}