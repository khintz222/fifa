package model

data class Player(
    var num: Int,
    var id: Int,
    var name: String,
    var age: Int,
    var photo: String,
    var nationality: String,
    var flag: String,
    var overall: Int,
    var potential: Int,
    var club: String,
    var clubLogo: String,
    var value: String,
    var wage: String
)